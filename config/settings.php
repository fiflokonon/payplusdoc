<?php

// Should be set to 0 in production
error_reporting(E_ALL);

// Should be set to '0' in production
ini_set('display_errors', '1');

// Settings
$settings = [];

// ...

// Twig settings
$settings['twig'] = [
// Template paths
    'paths' => [
        __DIR__ . '/../templates',
    ],
// Twig environment options
    'options' => [
// Should be set to true in production
        'cache_enabled' => false,
        'cache_path' => __DIR__ . '/../tmp/twig',
    ],
];
return $settings;