<?php
use Slim\App;
use Selective\BasePath\BasePathMiddleware;
use Slim\Views\TwigMiddleware;

return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    //Add base path middleware
    $app->add(BasePathMiddleware::class);

    //Add Twig Middleware
    $app->add(TwigMiddleware::class);

    // Catch exceptions and errors
    $app->addErrorMiddleware(true, true, true);
};